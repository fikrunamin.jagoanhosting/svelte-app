FROM node:16.16.0-alpine as build

WORKDIR /app
COPY ./package.json ./
COPY ./package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM node:16.16.0-alpine as deploy-node
WORKDIR /app
RUN rm -rf ./*
COPY --from=build /app/package.json .
COPY --from=build /app/build .
# NPM Install for production, deprecated => npm install --production
RUN npm install --omit=dev
EXPOSE 3000
CMD node index.js